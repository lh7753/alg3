import java.util.ArrayList;
import java.util.Random;

public class Genetic {

    public void geneticAlgorithm(Data data){
        //Initialization: Create initial population of N (W H) chromosomes; Evaluate chromosomes;
        ArrayList<Population> pop = randomGenerator(data.n, data.m, data.r, data.matrix);
        int best = 0;
        int vrednost = pop.get(0).fitness;

        while(true){

            //Select suitable chromosomes for reproduction (parents);
            Population suitable = rouletteWheel(pop);

            if(suitable.fitness > best){
                System.out.println(suitable.fitness - vrednost);
                best = suitable.fitness;
            }

            //Apply crossover on matrix W of selected parents;
            int[][] crossMatrix1 = crossover(suitable.matrix1, suitable.matrix2);

            //Mutate W with very small probability; Mutate H;
            int[][] mutateM2 = mutate(suitable.matrix2, 3);
            int[][] mutateM1 = mutate(crossMatrix1, 0);

            //Migrate offspring chromosomes to current population; Evaluate current population using objective function;
            pop.add(new Population(crossMatrix1, suitable.matrix2, suitable.result, calculateFitness(multiply(mutateM1, mutateM2), suitable.result)));
        }
    }




    public int[][] crossover(int[][] matrix1, int[][] matrix2){
        Random random = new Random();
        int n = (matrix1.length * 15) / 100;

        int[] koordinateM1 = new int[n];
        int[] koordinateM2 = new int[n];

        for(int i = 0; i < n; i++){
            koordinateM1[i] = random.nextInt(matrix1.length);
            koordinateM2[i] = random.nextInt(matrix2[0].length);
        }

        for(int i = 0; i < n; i++){
            for(int j = 0; j < matrix1[koordinateM1[i]].length; j++){
                matrix1[koordinateM1[i]][j] = matrix2[j][koordinateM2[i]];
            }
        }


        return matrix1;
    }

    public int[][] mutate(int[][] matrix, int probability){
        Random random = new Random();
        int stMenjav = (matrix.length * matrix[0].length * probability) / 100;
        int a = random.nextInt(matrix.length);
        int b = random.nextInt(matrix[0].length);

        for(int i = 0; i < stMenjav; i++){
            if(matrix[a][b] == 1){
                matrix[a][b] = 0;
            }else{
                matrix[a][b] = 1;
            }
        }
        return matrix;
    }

    //vrne matriko glede na njeno verjetnost (fitness)
    public Population rouletteWheel(ArrayList<Population> pop){
        int totalSum = 0;
        int partialSum = 0;

        for(Population p : pop){
            totalSum += p.fitness;
        }

        Random random = new Random();
        int randomValue = random.nextInt(totalSum);

        for(Population p : pop){
            partialSum += p.fitness;
            if(partialSum > randomValue){
                return p;
            }
        }
        return null;
    }

    public int calculateFitness(int[][] composed, int[][] result){
        int counter = 0;
        for(int i = 0; i < composed.length; i++){
            for(int j = 0; j < composed[i].length; j++){
                if(composed[i][j] == result[i][j]){
                    counter ++;
                }
            }
        }
        return counter;
    }

    public int[][] multiply(int[][] matrix1, int[][] matrix2){
        int[][] composed = new int[matrix1.length][matrix2[0].length];

        for(int i = 0; i < matrix1.length; i++){
            for(int j = 0; j < matrix2[0].length; j++){
                for(int k = 0; k < matrix1[0].length; k++){
                    if(matrix1[i][k] == 1 && matrix2[k][j] == 1){
                        composed[i][j] = 1;
                        break;
                    }else{
                        composed[i][j] = 0;
                    }
                }
            }

        }

        return composed;
    }

    public ArrayList<Population> randomGenerator(int n, int m, int r, int[][] result){
        ArrayList<Population> pop = new ArrayList<>();

        int[][] matrix1 = randomMatrix(n, r);
        int[][] matrix2 = randomMatrix(r, m);

        int[][] composed = multiply(matrix1, matrix2);

        int fitness = calculateFitness(composed, result);

        pop.add(new Population(matrix1, matrix2, result, fitness));

        return pop;
    }
    public int[][] randomMatrix(int n, int m){
        int[][] matrix = new int[n][m];
        Random random = new Random();

        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[0].length; j++){
                matrix[i][j] = random.nextInt(2);
            }
        }

        return matrix;
    }


    public static void izpis(int[][] matrix){
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[i].length; j++){
                System.out.print(matrix[i][j]);
            }
            System.out.println(" ");
        }
    }
}

