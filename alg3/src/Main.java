public class Main {

    public static void main(String[] args) {

        ReadFile r = new ReadFile();
        r.openFile(args[0]);
        Data data = r.readFile();
        r.closeFile();

        Genetic g = new Genetic();
        /*int[][] m1 = {{1, 1, 0}, {0, 1, 0}, {0, 0, 1}};
        int[][] m2 = {{1, 0, 0}, {1, 1, 1}, {0, 0, 1}};


        int[][] m3 = {{1, 0, 0, 1}, {0, 1, 0, 1}, {1, 1, 1, 1}};
        int[][] m4 = {{1, 0}, {0, 1}, {1, 1}, {1, 0}};*/


        //int[][] rez = g.multiply(m1, m2);

        //izpis(rez);
        /*for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[i].length; j++){
                System.out.print(matrix[i][j]);
            }
            System.out.println();
        }*/


        g.geneticAlgorithm(data);


    }

    public static void izpis(int[][] matrix){
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[i].length; j++){
                System.out.print(matrix[i][j]);
            }
            System.out.println(" ");
        }
    }
}
