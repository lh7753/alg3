public class Data {
    int[][] matrix;
    int m;
    int n;
    int r;

    public Data(int[][] matrix, int m, int n, int r){
        this.matrix = matrix;
        this.m = m;
        this.n = n;
        this.r = r;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }
}
