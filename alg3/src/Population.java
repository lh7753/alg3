public class Population {
    int[][] matrix1;
    int[][] matrix2;
    int[][] result;
    int fitness;

    public Population(int[][] matrix1, int[][] matrix2){
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
    }

    public Population(int[][] matrix1, int[][] matrix2, int[][] result, int fitness){
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
        this.result = result;
        this.fitness = fitness;
    }


    public int[][] getMatrix1() {
        return matrix1;
    }

    public void setMatrix1(int[][] matrix1) {
        this.matrix1 = matrix1;
    }

    public int[][] getMatrix2() {
        return matrix2;
    }

    public void setMatrix2(int[][] matrix2) {
        this.matrix2 = matrix2;
    }

    public int[][] getResult() {
        return result;
    }

    public void setResult(int[][] result) {
        this.result = result;
    }

    public int getFitness() {
        return fitness;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }
}
