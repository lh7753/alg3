import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadFile {

    Scanner file;

    public void openFile(String title){
        try{
            file = new Scanner(new File(title)) ;
        }catch (Exception e){
            System.out.println("Could not find file!");
        }
    }

    public Data readFile(){

        int n = Integer.parseInt(file.next());
        int  m = Integer.parseInt(file.next());
        int r = Integer.parseInt(file.next());
        String matrix = "";

        while(file.hasNextLine()){
            matrix = file.nextLine();
        }

        return new Data(compose(n, m, matrix), m, n, r);

    }

    private int[][] compose(int n, int m, String mtrx){
        int[][] matrix = new int[n][m];
        char[] mt = mtrx.toCharArray();

        int counter = 0;

        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[i].length; j++){
                matrix[i][j] = Character.getNumericValue(mt[counter]);
                counter++;
            }
        }

        return matrix;
    }

    public void closeFile(){
        file.close();
    }
}